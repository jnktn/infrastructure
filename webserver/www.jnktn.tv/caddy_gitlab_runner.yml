---
# Playbook to install Caddy and Gitlab runner on Ubuntu:22.04 and
# register it to gitlab.com instance for jnktn/jntkn.tv project
# registration token "registration_token" must be set in group_vars/gitlab_runner_creds.yml
# without "registration_token" variable, several steps will be skipped!

- name: Install Caddy and GitLab runner
  hosts: www_jnktn
  become: true
  vars_files:
    - group_vars/gitlab_runner_creds.yml
  tasks:
    - name: Upgrade existing system packages
      ansible.builtin.apt:
        upgrade: true
        update_cache: true

    - name: Install required system packages
      ansible.builtin.apt:
        pkg:
          - apt-transport-https
          - ca-certificates
          - curl
          - gnupg
          - ntp
          - tzdata
          - python3-pip
        state: latest

    - name: Set timezone to Europe/Berlin
      community.general.timezone:
        name: Europe/Berlin

    - name: Start NTP service
      ansible.builtin.service:
        name: ntp
        enabled: true
        state: started

    - name: Add Caddy GPG apt Key
      ansible.builtin.apt_key:
        url: https://dl.cloudsmith.io/public/caddy/stable/gpg.key
        state: present

    - name: Add Caddy apt Repository
      ansible.builtin.apt_repository:
        repo: deb https://dl.cloudsmith.io/public/caddy/stable/deb/debian any-version main
        state: present

    - name: Update apt and install Caddy
      ansible.builtin.apt:
        name: caddy
        state: latest
        update_cache: true

    - name: Update Caddyfile
      ansible.builtin.copy:
        src: ./Caddyfile
        dest: /etc/caddy/Caddyfile
        owner: caddy
        mode: '644'

    - name: Reload Caddy with new configurations
      ansible.builtin.service:
        name: caddy
        state: reloaded

    - name: Add GitLab GPG apt Key
      ansible.builtin.apt_key:
        url: https://packages.gitlab.com/runner/gitlab-runner/gpgkey
        state: present

    - name: Add GitLab apt Repository
      ansible.builtin.apt_repository:
        repo: deb https://packages.gitlab.com/runner/gitlab-runner/ubuntu/ jammy main
        state: present

    - name: Install Gitlab runner
      ansible.builtin.apt:
        name: gitlab-runner
        state: latest
        update_cache: true

    - name: Create website directory
      ansible.builtin.file:
        path: /var/www/jnktn.tv
        state: directory
        group: gitlab-runner
        owner: gitlab-runner
        mode: '755'

    - name: "Unregister existing Gitlab runners"
      ansible.builtin.command: gitlab-runner unregister --all-runners
      changed_when: true
      when: registration_token is defined

    - name: "Register Gitlab runner"
      ansible.builtin.command: gitlab-runner register --non-interactive \
        --url https://gitlab.com --token "{{ registration_token }}" \
        --description 'www.jnktn Runner' --executor 'shell'
      changed_when: true
      when: registration_token is defined

    - name: "Restart gitlab runner service"
      ansible.builtin.command: gitlab-runner restart
      changed_when: false
