## Install / update webserver (Caddy + GitLab CI runner)
0. prerequisites:
    - [ansible](https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html) (installed on your local machine)
    - an Ubuntu LTS VM which you can connect to via SSH using public key authentication as root or a user with sudo permission. DNS record for (www.jntkn.tv & jnktn.tv) must be pointing to the IP of this VM. Incoming traffic to ports 80 and 443 must be allowed.
1. [optional for updates] generate a gitlab CI [runner authentication token](https://docs.gitlab.com/ee/ci/runners/runners_scope.html#create-a-project-runner-with-a-runner-authentication-token) for [jnktn.tv project on gitlab.com](https://gitlab.com/jnktn/jnktn.tv/). This runner must be "protected" and must be tagged as "www_jnktn". Save the token in `group_vars/gitlab_runner_creds.yml` file as:

    ```yaml
    ---
    registration_token: glrt-xxxxxxxxxx
    ```

2. run ansible script

   ```bash
    ansible-playbook -v caddy_gitlab_runner.yml
    ```
