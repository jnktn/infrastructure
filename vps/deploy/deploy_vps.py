import time
from hcloud import Client
from hcloud.images.domain import Image
from hcloud.server_types.domain import ServerType

# Hetzner API token for project - hardcode or ask user
# api_token = 'CHANGE ME'
api_token = input("Hetzner API token: ")
client = Client(token=api_token)

# SSH key for project - hardcode or ask user
# ssh_key_name = 'CHANGE ME'
ssh_key_name = input("Name of SSH key on account: ")
ssh_key = client.ssh_keys.get_all(name=ssh_key_name)

server_name = ""

server_location = input("Which location? (usw/use/de): ")

if server_location == "usw":

    server_name = "test.uswest.jnktn.tv"

    location = client.locations.get_by_name("hil")

    response = client.servers.create(
        name=server_name,
        server_type=ServerType("cpx11"),
        image=Image(name="rocky-9"),
        location=location,
        ssh_keys=ssh_key,
    )

elif server_location == "use":

    server_name = "test.useast.jnktn.tv"

    location = client.locations.get_by_name("ash")

    response = client.servers.create(
        name=server_name,
        server_type=ServerType("cpx11"),
        image=Image(name="rocky-9"),
        location=location,
        ssh_keys=ssh_key,
    )

elif server_location == "de":

    server_name = "test.de.jnktn.tv"

    location = client.locations.get_by_name("nbg1")

    response = client.servers.create(
        name=server_name,
        server_type=ServerType("cpx11"),
        image=Image(name="rocky-9"),
        location=location,
        ssh_keys=ssh_key,
    )

else:
    print("Invalid option. Exiting.")


new_server_ip = client.servers.get_by_name(server_name).public_net.ipv4.ip
print(new_server_ip)
